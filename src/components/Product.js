import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Container from '@material-ui/core/Container';
import Chip from '@material-ui/core/Chip';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import CheckCircle from '@material-ui/icons/CheckCircle';

import { Button, Avatar } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    
  },
  toolbarLink: {
    padding: theme.spacing(1),
    flexShrink: 0,
  },
  thumbnailSection: {
    backgroundColor: '#f5f5f5',
    paddingTop: theme.spacing(5),
    paddingBottom: theme.spacing(5),
  },
  productName: {
    fontWeight: 600
  },
  productCategory: {
    height: 20,
    fontSize: 10,
    borderRadius: 3,
    marginRight: 2,
    padding: 0.5
  },
  productTitle: {
    fontSize: 15,
    marginTop: theme.spacing(3)
  },
  productDescription: {
    marginTop: theme.spacing(1)
  },
  productUrl: {
    marginTop: theme.spacing(2),
    display: 'block'
  },
  productLeft: {
    marginTop:  theme.spacing(5)
  },
  priceDetails: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  priceNew: {
    fontSize: 36,
    color: '#ec3646',
    fontWeight: 700
  },
  priceOld: {
    marginLeft: theme.spacing(1),
    textDecoration: 'line-through',
    color: 'gray'
  },
  priceDiscount: {
    height: 24,
    background: '#fdd767',
    borderRadius: 3,
    marginLeft: theme.spacing(1)
  },
  buyNow: {
    color: 'white',
    background: '#de5864',
    borderRadius: '24px',
    '&:hover': {
      background: '#d0211c'
    }
  },
  recommend: {
    marginLeft: theme.spacing(1),
    borderRadius: '24px',
    background: '#4575d1',
    color: 'white'
  },
  paperGeneral: {
    padding: theme.spacing(3),
    marginBottom: theme.spacing(2)
  },
  paperName: {
    fontWeight: 600
  },
  memberItem: {
    marginTop: theme.spacing(2)
  },
  avatarPhoto: {
    display: 'inline-block',
    marginRight: theme.spacing(1),
    verticalAlign: 'middle'
  },
  memberDescription: {
    display: 'inline-block',
    verticalAlign: 'middle'
  },
  avatarName: {
    fontSize: 14,
    fontWeight: 600
  },
  avatarTitle: {
    fontSize: 14,
    color: 'gray'
  },
  planTitle: {
    fontSize: 36,
    color: '#ec3646',
    fontWeight: 700,
    marginTop: theme.spacing(2)
  },
  planDescription: {
    marginBottom: theme.spacing(2)
  },
  featureItem: {
    marginTop: theme.spacing(1)
  },
  featureCheck: {
    color: "#ec3646",
    verticalAlign: "middle"
  },
  featureLine: {
    marginLeft: theme.spacing(1),
    fontSize: 12,
    display: 'inline-block',
    verticalAlign: 'middle'
  },
  buyNow_W: {
    width: '100%',
    marginTop: theme.spacing(2)
  },
  sales_count: {
    textAlign: 'center',
    fontSize: 12,
    marginTop: theme.spacing(2)
  }
}));

const sections = [
  'Feed',
  'Discover',
  'Community',
  'Deals',
];

const productDetails = {
    name: 'Personizely',
    image: 'https://source.unsplash.com/user/erondu',
    price: '$49',
    discount: '$297/yearly',
    sale_percent: '94% OFF',
    title: 'A Conversion Marketing Toolkit',
    description: 'A much-needed tool for converting your new visitors to paying and loyal customers with a unique level of personalization on your website',
    url: 'https://www.personizely.net',
    recommend: '149',
    categories: [
        'Saas Product',
        'Marketing',
        'Developer Tools',
    ],
    sales_available: '22',
    reviews: '34',
    discussion: '10',
}

const productTeam = [
    {
        name: 'Lukas Liesis',
        title: 'CTRO @PitchGround',
        photo: 'https://source.unsplash.com/random',
    },
    {
        name: 'Udit Goenka',
        title: 'CEO @PitchGround',
        photo: 'https://source.unsplash.com/random',
    },
]

const productPlans = [
    {
        name: 'Basic Plan',
        title: '$59/lifetime',
        description: 'A much-needed tool for converting your new visitors to paying and loyal customers with a unique level of personalization on your website.',
        features: [
            'Unlimited Access To All Features',
            '50k Unique Impressions',
            '5 Domains'
        ],
        sales_count: '219',
    },
    {
        name: 'Pro Plan',
        title: '$118/lifetime',
        description: 'A much-needed tool for converting your new visitors to paying and loyal customers with a unique level of personalization on your website.',
        features: [
            'Unlimited Access To All Features',
            '50k Unique Impressions',
            '5 Domains',
            'Branding Included',
            'Web Personalization',
            'Emai List Building',
        ],
        sales_count: '312',
    }
]

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

export default function Product() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }
  return (
    <React.Fragment>
      <Container maxWidth={false} style={{padding: '0'}}>
        <Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>
          {sections.map(section => (
            <Link
              color="inherit"
              noWrap
              key={section}
              variant="body2"
              href="#"
              className={classes.toolbarLink}
            >
              {section}
            </Link>
          ))}
        </Toolbar>
        <main>
          {/* Thumbnail section */}
          <section className={classes.thumbnailSection}>
              <Container maxWidth="lg">
                <Grid container spacing={4}>
                  <Grid item md={8}>
                    <img
                      style = {{width: '100%'}}                    
                      src={productDetails.image}
                      alt="background"
                    />
                  </Grid>
                  <Grid item md={4}>
                    <div>
                      <Typography component="h4" variant="h5" color="inherit" className={classes.productName}>
                        {productDetails.name}                    
                      </Typography>
                      {productDetails.categories.map(category => (
                        <Chip label={category} className={classes.productCategory}/>
                      ))}
                      <Typography component="h3" variant="h5" className={classes.productTitle}>{productDetails.title}</Typography>
                      <Typography variant="subtitle1" color="textSecondary" className={classes.productDescription}>
                        {productDetails.description}
                      </Typography>
                      <Link href={productDetails.url} className={classes.productUrl}>{productDetails.url}</Link>
                      <Typography variant="subtitle2" color="textSecondary" className={classes.productLeft}>
                        {productDetails.sales_available} coupons left
                      </Typography>
                      <div className={classes.priceDetails}>
                        <span className={classes.priceNew}>{productDetails.price}</span>
                        <span className={classes.priceOld}>{productDetails.discount}</span>
                        <Chip className={classes.priceDiscount} label={productDetails.sale_percent}/>
                      </div>                    
                      <Button variant="contained" className={[classes.buyNow, classes.buyNow_inline]}>
                        Buy Now
                      </Button>
                      <Button variant="contained" className={classes.recommend}>
                        Recommend
                      </Button>
                    </div>
                  </Grid>
                </Grid>
              </Container>
          </section>
          {/* Product detail section */}
          <section>
            <Container maxWidth="lg">
              <Tabs value={value} onChange={handleChange}>
                <Tab label="Details"/>
                <Tab label="Reviews"/>
                <Tab label="Discussions"/>
              </Tabs>
              <Grid container spacing={4} className={classes.cardGrid}>
                <Grid item md={8}>
                  {value === 0 && <TabContainer>Details</TabContainer>}
                  {value === 1 && <TabContainer>Reviews</TabContainer>}
                  {value === 2 && <TabContainer>Discussion</TabContainer>}
                </Grid>
                <Grid item md={4}>
                  <Paper className={classes.paperGeneral}>
                    <Typography component="h3" variant="subtitle1" className={classes.paperName}>Team</Typography>
                    <div className={classes.paperContent}>
                      {productTeam.map(member => (
                        <div className={classes.memberItem}>
                          <Avatar src={member.photo} className={classes.avatarPhoto} style={{display: 'inline-block'}}/>
                          <div className={classes.memberDescription}>
                            <Typography className={classes.avatarName}>{member.name}</Typography>
                            <Typography className={classes.avatarTitle}>{member.title}</Typography>
                          </div>
                        </div>
                      ))}
                    </div>
                  </Paper>
                  {productPlans.map(plan => (
                    <Paper className={classes.paperGeneral}>
                      <Typography component="h3" variant="subtitle1" className={classes.paperName}>{plan.name}</Typography>
                      <Typography className={classes.planTitle}>{plan.title}</Typography>
                      <Typography className={classes.planDescription} color="textSecondary">{plan.description}</Typography>
                      <Typography component="h3" variant="subtitle1" className={classes.paperName}>Features</Typography>
                      {plan.features.map(feature => (
                        <Typography className={classes.featureItem}>
                          <CheckCircle className={classes.featureCheck}></CheckCircle>
                          <Typography className={classes.featureLine}>{feature}</Typography>
                        </Typography>
                      ))}
                      <Button variant="contained" className={[classes.buyNow, classes.buyNow_W]}>
                        Buy Now
                      </Button>
                      <Typography color="textSecondary" className={classes.sales_count}>{plan.sales_count} user already bought it</Typography>
                    </Paper>
                  ))}
                  
                </Grid>
              </Grid>
            </Container>        
          </section>          
        </main>
      </Container>
    </React.Fragment>
  );
}