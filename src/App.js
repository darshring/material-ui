import React from 'react';
import Product from './components/Product';

import './App.css';

function App() {
  return (
    <div>
      <Product sample="SAMPLE"/>
    </div>
  );
}

export default App;
